import React from 'react';

class ProductCategoryRow extends React.Component {
  render() {
    return (
      <ul>
        <b>{this.props.category}</b>
        {this.props.children}
      </ul>
    );
  }
}

export default ProductCategoryRow;
