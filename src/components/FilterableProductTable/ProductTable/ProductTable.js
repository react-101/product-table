import React from 'react';
import ProductCategoryRow from './ProductCategoryRow/ProductCategoryRow';
import ProductRow from './ProductRow/ProductRow';

const groupByKeyToObject = (array, key) => {
  const obj = {};
  array.forEach(element => {
    const group = obj[element[key]];
    if (!group) {
      obj[element[key]] = [element];
    } else {
      obj[element[key]].push(element);
    }
  });

  return obj;
};

class ProductTable extends React.Component {
  render() {
    console.log(this.props.inStockOnly);
    let filteredModel = this.props.model.filter(e =>
      this.props.inStockOnly
        ? e.name.toLowerCase().includes(this.props.filterText) && e.stocked === this.props.inStockOnly
        : e.name.toLowerCase().includes(this.props.filterText),
    );
    let groupedProductsByCategory = groupByKeyToObject(filteredModel, 'category');
    let categories = Object.keys(groupedProductsByCategory).map(key => {
      return key;
    });

    return (
      <div>
        {categories.map(e => {
          return (
            <ProductCategoryRow category={e} key={e}>
              {Object.values(groupedProductsByCategory[e]).map(element => (
                <ProductRow product={element} key={element.name} />
              ))}
            </ProductCategoryRow>
          );
        })}
      </div>
    );
  }
}

export default ProductTable;
