import React from 'react';

class ProductRow extends React.Component {
  render() {
    return (
      <li>
        {this.props.product.name} - {this.props.product.price}
      </li>
    );
  }
}

export default ProductRow;
