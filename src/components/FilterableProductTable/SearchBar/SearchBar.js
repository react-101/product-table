import React from 'react';

class SearchBar extends React.Component {
  render() {
    return (
      <div>
        <input type="text" value={this.props.filterText} onChange={this.props.onTextChange} />
        <input type="checkbox" checked={this.props.inStockOnly} onChange={this.props.onCheckboxChange} />
      </div>
    );
  }
}

export default SearchBar;
