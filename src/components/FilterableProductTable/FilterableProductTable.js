import React from 'react';
import SearchBar from './SearchBar/SearchBar';
import ProductTable from './ProductTable/ProductTable';

class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
      inStockOnly: false,
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
  }

  handleTextChange(e) {
    this.setState({
      filterText: e.target.value,
    });
    // console.log(e.target.value);
  }

  handleCheckboxChange(e) {
    this.setState({
      inStockOnly: this.state.inStockOnly ? false : true,
    });
  }
  render() {
    const model = [
      { category: 'Sporting Goods', price: '$9.9', stocked: true, name: 'Football' },
      { category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball' },
      { category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball' },
      { category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch' },
      { category: 'Electronics', price: '$999.99', stocked: true, name: 'iPod Touch PRO' },
      { category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5' },
      { category: 'Sporting Goods', price: '$99.99', stocked: true, name: 'Baseball UP' },
      { category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7' },
    ];

    return (
      <div className="filterable-product-table">
        <SearchBar
          filterText={this.state.filterText}
          inStockOnly={this.state.inStockOnly}
          onTextChange={this.handleTextChange}
          onCheckboxChange={this.handleCheckboxChange}
        />
        <ProductTable model={model} filterText={this.state.filterText} inStockOnly={this.state.inStockOnly} />
      </div>
    );
  }
}

export default FilterableProductTable;
